FROM ubuntu:20.04
LABEL maintainer="Nimit"
LABEL version="0.1"
LABEL description="This is custom Docker Image."
ENV DEBIAN_FRONTEND=nonintercative
RUN apt-get update -y
RUN apt-get install wget -y
RUN apt-get install gnupg -y
RUN apt-get install sudo -y
RUN apt-get install vim
RUN apt-get install -y ca-certificates
RUN wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
    RUN apt install postgresql postgresql-contrib -y
