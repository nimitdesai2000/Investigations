package org.nd;

public class EvenDigits {
    public static void main(String[] args) {
        int arr[] =new int[]{33,2,1,456,78975,34,56};
        int result = findEvenDigits(arr);
        System.out.println("No of even digits are : " + result);
    }

    private static int findEvenDigits(int[] arr) {
        int count=0;
        for (int i=0;i<arr.length;i++) {
            int result = isEvenDigit(arr[i]);
            if((result%2)==0)
                count++;
        }
        return count;
    }

    private static int isEvenDigit(int i) {
        int count=0;
        while(i>0) {
            i=i/10;
            count++;
        }
        return count;
    }
}
