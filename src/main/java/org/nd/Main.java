package org.nd;
public class Main {
    public static void main(String[] args) {
        System.out.println("-----------Programme stating---------");
        try {
            performExceptionHandling();
            System.out.println("After malicious function call");
        }
        catch (Exception e)  {
            System.out.println("Parent "+e);
        }
        System.out.println("After try from parent");
    }

    public static void performExceptionHandling() {
            System.out.println(10/0);
        System.out.println("From child after malicious code");
    }
}