package org.nd;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.List;

// Code
public class MaxMin {
    public static void main(String[] a) {
        int[] arr = {55,66,88,22,44,66,77,88,45,74,54};
        arr = new int[]{44, 56, 76, 45, 34, 65, 43, 46, 87};
        int[] result = findNthMaxMin(arr,3);
        System.out.println("Nth min value is : " + result[0]);
        System.out.println("Nth max value is : " + result[1]);
        //callsGC();

    }

    private static void callsGC() {
        System.gc();

        try {
            List<GarbageCollectorMXBean> gcMxBeans = ManagementFactory.getGarbageCollectorMXBeans();

            for (GarbageCollectorMXBean gcMxBean : gcMxBeans) {
                System.out.println(gcMxBean.getName());
                System.out.println(gcMxBean.getObjectName());
            }
        } catch (RuntimeException re) {
            throw re;
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }

    public static int[] findNthMaxMin(int[] arr,int n) {
        Arrays.sort(arr);
        int arr1[]= new int[2];
        arr1[0] = arr[n-1];
        arr1[1] = arr[arr.length-n];
        return(arr1);
    }
}
