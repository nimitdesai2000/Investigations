package org.nd.SearchingAlgo;

import java.util.Scanner;

public class BinarySearch {
    public static void main(String[] args) {
       // int[] arr = new int[]{2,3,5,7,8,9,14,16,23,45,67,89};
        int[] arr = new int[]{89,67,45,23,16,14,9,8,7,5,3,2};
        String order = findArraySortingOrder(arr);
        int l = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("----Enter number to search in Array using binary search----");
        int k =scanner.nextInt();
        int r = arr.length-1;
        int result = findElementUsingBinarySearch(arr,l,r,k,order);
        System.out.println(result);
    }

    private static String findArraySortingOrder(int[] arr) {
        if(arr[0] <= arr[arr.length-1])
            return "Ascending";
        else
            return "Descending";
    }

    private static int findElementUsingBinarySearch(int[] arr, int l, int r, int k, String order) {

        while(l<=r) {
            int mid = l + (r - l) / 2;
            if(order == "Ascending") {
                System.out.println("Your array is in Ascending order");
                if (k == arr[mid])
                    return mid;
                else if (k < arr[mid])
                    r = mid - 1;
                else if (k > arr[mid])
                    l = mid + 1;
            }
            else if (order == "Descending") {
               // System.out.println("Your array is in Descending order");
                if (k == arr[mid])
                    return mid;
                else if (k < arr[mid])
                    l = mid + 1;
                else if (k > arr[mid])
                    r = mid - 1;
            }
        }
        return -1;
    }
}
